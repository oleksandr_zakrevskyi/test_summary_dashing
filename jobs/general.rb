t = Time.now.utc.to_i

interval = 5
idling_frequence = 0.2
middle_speed = 11

concurrency = 100
series_trips = []
series_engine_hours = []
series_idling = []
series_mileage = []
series_speeding = []
series_pto = []
ii = []

concurrency.times do |id|
  series_trips[id] = [{ name: "Finished:", data: [{x:t, y:0}] }, { name: "In progress:", data: [{x:t, y:0}] }]
  series_engine_hours[id] = [{ name: "Total Engine Hours:", data: [{x:t, y:0}] }, { name: "Maximum Engine Hours:", data: [{x:t, y:0}] }]
  series_idling[id] = [{ name: "Total Idling:", data: [{x:t, y:0}] }, { name: "Maximum Idling:", data: [{x:t, y:0}] }]
  series_mileage[id] = [{ name: "Total Mileage:", data: [{x:t, y:0}] }, { name: "Maximum Mileage:", data: [{x:t, y:0}] }]
  series_speeding[id] = [{ name: "Total Speeding Alerts:", data: [{x:t, y:0}] }, { name: "Maximum Speed:", data: [{x:t, y:0}] }]
  series_pto[id] = [{ name: "Total PTO Alerts:", data: [{x:t, y:0}] }, { name: "Last Event From:", data: [{x:t, y:0}] }]
  ii[id] = 0
end

SCHEDULER.every "#{interval}s" do
  concurrency.times do |id|
    t = Time.now.utc.to_i
    if ii[id] == 0
      series_trips[id] = [{ name: "Finished:", data: [{x:t, y:0}] }, { name: "In progress:", data: [{x:t, y:0}] }]
      series_engine_hours[id] = [{ name: "Total Engine Hours:", data: [{x:t, y:0}] }, { name: "Maximum Engine Hours:", data: [{x:t, y:0}] }]
      series_idling[id] = [{ name: "Total Idling:", data: [{x:t, y:0}] }, { name: "Maximum Idling:", data: [{x:t, y:0}] }]
      series_mileage[id] = [{ name: "Total Mileage:", data: [{x:t, y:0}] }, { name: "Maximum Mileage:", data: [{x:t, y:0}] }]
      series_speeding[id] = [{ name: "Total Speeding Alerts:", data: [{x:t, y:0}] }, { name: "Maximum Speed:", data: [{x:t, y:0}] }]
      series_pto[id] = [{ name: "Total PTO Alerts:", data: [{x:t, y:0}] }, { name: "Last Event From:", data: [{x:t, y:0}] }]
    end
    ii[id] += 1
    ii[id] = 0 if ii[id] > 100

    last_finished = series_trips[id][0][:data].last
    last_progress = series_trips[id][1][:data].last
    new_trips = rand(2)
    is_finished = new_trips == 1 && rand(2) == 0 ? rand(last_progress[:y] + 1) : 0

    series_trips[id][0][:data].shift if series_trips[id][0][:data].length >= 10
    #series[0][:data] << { x: last_finished[:x] + 3600, y: last_finished[:y] + is_finished }

    series_trips[id][0][:data] << { x: t, y: last_finished[:y] + is_finished }
    series_trips[id][1][:data].shift if series_trips[id][1][:data].length >= 10
    series_trips[id][1][:data] << { x: t, y: last_progress[:y] + new_trips - is_finished }

    delta_engine_hours = last_progress[:y] * interval
    last_total_engine = series_engine_hours[id][0][:data].last
    last_max_engine = series_engine_hours[id][1][:data].last

    series_engine_hours[id][0][:data].shift if series_engine_hours[id][0][:data].length >= 10
    series_engine_hours[id][0][:data] << { x: t, y: last_total_engine[:y] + delta_engine_hours }

    max_engine = delta_engine_hours > 0 ? last_max_engine[:y] + rand(2)*interval : 0
    max_engine = last_max_engine[:y] if  last_max_engine[:y].to_i > max_engine

    series_engine_hours[id][1][:data].shift if series_engine_hours[id][1][:data].length >= 10
    series_engine_hours[id][1][:data] << { x: t, y: max_engine }

    delta_idling = (last_progress[:y] * interval * idling_frequence).to_i
    last_total_idling = series_idling[id][0][:data].last
    last_max_idling = series_idling[id][1][:data].last

    series_idling[id][0][:data].shift if series_idling[id][0][:data].length >= 10
    series_idling[id][0][:data] << { x: t, y: last_total_idling[:y] + delta_idling }

    max_idling = delta_idling > 0 ? (last_max_idling[:y] + rand(2)*interval* idling_frequence).to_i : 0
    max_idling = last_max_idling[id][:y] if  last_max_idling[:y].to_i > max_idling

    series_idling[id][1][:data].shift if series_idling[id][1][:data].length >= 10
    series_idling[id][1][:data] << { x: t, y: max_idling }

    series_mileage[id][0][:data].shift if series_mileage[id][0][:data].length >= 10
    series_mileage[id][0][:data] << { x: t, y: series_engine_hours[id][0][:data].last[:y] * middle_speed }
    series_mileage[id][1][:data].shift if series_mileage[id][1][:data].length >= 10
    series_mileage[id][1][:data] << { x: t, y: series_engine_hours[id][1][:data].last[:y] * middle_speed }

    speeding_alerts = series_speeding[id][0][:data].last[:y]
    max_speed = series_speeding[id][1][:data].last[:y]
    speeding = (rand(2) == 1)
    if speeding
      speeding_alerts += 1
      speed = rand(20..50)
      max_speed = speed if speed > max_speed
    end

    series_speeding[id][0][:data].shift if series_speeding[id][0][:data].length >= 10
    series_speeding[id][0][:data] << { x: t, y: speeding_alerts }
    series_speeding[id][1][:data].shift if series_speeding[id][1][:data].length >= 10
    series_speeding[id][1][:data] << { x: t, y: max_speed }

    pto_count = series_pto[id][0][:data].last[:y]
    last_event = series_pto[id][1][:data].last[:y]
    pto = (rand(2) == 1)
    if pto
      pto_count +=1
      last_event = t
    end

    series_pto[id][0][:data].shift if series_pto[id][0][:data].length >= 10
    series_pto[id][0][:data] << { x: t, y: pto_count }
    series_pto[id][1][:data].shift if series_pto[id][1][:data].length >= 10
    series_pto[id][1][:data] << { x: t, y: last_event }

    send_event("trips_#{id}", series: series_trips[id], value_0: series_trips[id][0][:data].last[:y], value_1: series_trips[id][1][:data].last[:y])

    send_event("engine_hours_#{id}", series: series_engine_hours[id], value_0: WidjectHelper::format_seconds(series_engine_hours[id][0][:data].last[:y]), value_1: WidjectHelper::format_seconds(series_engine_hours[id][1][:data].last[:y]))

    send_event("idling_#{id}", series: series_idling[id], value_0: WidjectHelper::format_seconds(series_idling[id][0][:data].last[:y]), value_1: WidjectHelper::format_seconds(series_idling[id][1][:data].last[:y]))

    send_event("mileage_#{id}", series: series_mileage[id], value_0: "#{WidjectHelper::format_distance(series_mileage[id][0][:data].last[:y])} km", value_1: "#{WidjectHelper::format_distance(series_mileage[id][1][:data].last[:y])} km")

    send_event("speeding_#{id}", series: series_speeding[id], value_0: series_speeding[id][0][:data].last[:y], value_1: "#{WidjectHelper::report_format_speed(series_speeding[id][1][:data].last[:y])} km/h")

    send_event("pto_#{id}", series: series_pto[id], value_0: series_pto[id][0][:data].last[:y], value_1: Time.parse(Time.parse("19700101").strftime('%FT%TZ')) + series_pto[id][1][:data].last[:y])
  end
end