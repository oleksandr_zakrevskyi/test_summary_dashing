module WidjectHelper
  def self.format_seconds(total_seconds)
    total_seconds = total_seconds.to_i
    format_str = ''
    data = []
    if (days = (total_seconds / (60 * 60 * 24)).to_i) > 0
      format_str += "%2dd "
      data << days
    end
    if (hours = (total_seconds / (60 * 60) % 24).to_i) > 0 || days > 0
      format_str += "%02dh "
      data << hours
    end
    if (minutes = (total_seconds / 60 % 60).to_i) > 0 || days > 0 || hours > 0
      format_str += "%02dm "
      data << minutes
    end
    if (seconds = (total_seconds % 60).to_i) > 0 || days > 0 || hours > 0 || minutes > 0
      format_str += "%02ds"
      data << seconds
    end
    format_str % data
  end

  def self.format_distance(distance, imperial = false, units = 'm')
    v = distance.to_f
    v = v / 1000.0 if units == 'm' || units.blank?
    if v > 0.0
      v = v.to_unit('km').convert_to('mile').scalar if imperial
      '%.2f' % v
    end
  end

  def self.report_format_speed(speed, imperial = false)
    speed = speed.to_f
    if (v = (speed * 3.6).round) > 0
      v = v.to_unit('km').convert_to('mile').scalar if imperial
      ('%0.2f' % v.round)
    end
  end
end