class Dashing.Summary extends Dashing.Widget

#  @accessor 'value_0', ->
#    return @get('print_value_0') if @get('print_value_0')
#    series = @get('series')
#    if series
#      series[0].data[series[0].data.length - 1].y
#    else
#      0
#
#  @accessor 'value_1', ->
#    return @get('print_value_1') if @get('print_value_1')
#    series = @get('series')
#    if series
#      series[1].data[series[1].data.length - 1].y
#    else
#      0

  ready: ->
    Dashing.debugMode = true
#    debugger;
    container = $(@node).parent()
    # Gross hacks. Let's fix this.
    width = (Dashing.widget_base_dimensions[0] * container.data("sizex")) + Dashing.widget_margins[0] * 2 * (container.data("sizex") - 1) - 20
    height = (Dashing.widget_base_dimensions[1] * container.data("sizey"))
    if @get('include_graph')
      @graph = new Rickshaw.Graph(
        element: @node
        width: width
        height: height
        renderer: 'line'
        series: [
          {
            color: "#000000",
            data: [{x:0,y:0}]
          },
          {
            color: "#ff0000",
            data: [{x:0,y:0}]
          }
        ]
        padding: {top: 0.02, left: 0.02, right: 0.02, bottom: 0.02}
      )
      @graph.series[0].data = @get('series')[0].data if @get('series')

      if @get('series_length') != 1
        @graph.series[1].data = @get('series')[1].data if @get('series')

      x_axis = new Rickshaw.Graph.Axis.Time(graph: @graph)
      y_axis = new Rickshaw.Graph.Axis.Y(graph: @graph, tickFormat: Rickshaw.Fixtures.Number.formatKMBT) if @get('y_axis_need')
      @graph.render()

  onData: (data) ->
    if data.value_0
      @value_0 = data.value_0
    if data.value_1
      @value_1 = data.value_1
    if @graph
      @graph.series[0].data = data.series[0].data
      if @get('series_length') != 1
        @graph.series[1].data = data.series[1].data
      @graph.render()